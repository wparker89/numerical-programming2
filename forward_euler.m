function y = forward_euler(f, y0, h, n)
% f - The function you wish to solve the initial value problem for.
% y0 - The initial values of the system of equations.
% h - The step size between evaluations of f.
% n - The number you want to evaluate y at e.g y(4) => n = 4.

steps = n / h; % Determine how many steps we need.
y = zeros(size(y0, 1), steps);
y(:, 1) = y0; % Set initial values

for i=1:steps
    y(:, i + 1) = y(:, i) + h * f(y(:, i));
end

end