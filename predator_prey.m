% Set problem parameters
a1 = 1; a2 = 1; a3 = 1; a4 = 1;

% Create the system of differential equations
f = @(x)[a1 * x(1, :) - a2 * x(2, :) * x(1, :);
         -a3 * x(2, :) + a4 * x(1, :) * x(2, :)];
     
% Set the initial condtions
y0 = [0.9; 0.9];

% Set the step size
h = 0.1;

% Calculate points
y = forward_euler(f, y0, h, 10)

x = linspace(0,100, 101);
plot(x, y(1,:), x, y(2, :))
legend('prey', 'predator')